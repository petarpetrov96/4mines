# 4Mines

Inspired by the world famous game Minesweeper, 4Mines is a simple puzzle with a similar concept. We have a grid and a couple of mines and the goal of the game is to avoid all mines and open the rest of the blocks. 

The rules are simple:
- Left clicking:
  - opens a cell if the cell was previously closed or
  - opens all of the non-flagged direct neighbours (left, top, right, bottom) if the cell was previously open
- Right clicking flags/unflags a cell (marking it as a possible mine) and prevents the user from opening it
- The game is won when all cells that are not mines are opened
- Opening a cell with a mine automatically finishes the game with a loss

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.
