import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders the page container', () => {
  render(<App />);
  const pageContainer = screen.getByTestId('pageContainer');
  expect(pageContainer).toBeInTheDocument();
});
