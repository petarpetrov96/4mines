import React from 'react';
import { ApertureOutline } from 'react-ionicons';
import Dropdown from '../ui-components/Dropdown';
import Link from '../ui-components/Link';
import STYLES from './SettingsBar.module.css';
import SettingsBarEntry from './SettingsBarEntry';

interface SettingsBarProps {
    scaling: string,
    setScaling: (scaling: string) => void,
    scalingOptions: string[],
    changeDifficulty: (difficulty: number) => void
}

const SettingsBar: React.FunctionComponent<SettingsBarProps> = ({
    scaling,
    setScaling,
    scalingOptions,
    changeDifficulty
}: SettingsBarProps) => {
    return (
        <div className={STYLES.settingsBar}>
            <SettingsBarEntry>
                <Dropdown
                    values={scalingOptions}
                    selectedValue={scaling}
                    setSelectedValue={setScaling}
                    CustomIcon={ApertureOutline}
                    title={'Zoom level'}
                />
            </SettingsBarEntry>
            <SettingsBarEntry>
                <Link onClick={() => changeDifficulty(0)}>Beginner</Link>
            </SettingsBarEntry>
            <SettingsBarEntry>
                <Link onClick={() => changeDifficulty(1)}>Intermediate</Link>
            </SettingsBarEntry>
            <SettingsBarEntry>
                <Link onClick={() => changeDifficulty(2)}>Expert</Link>
            </SettingsBarEntry>
        </div>
    )
};

export default SettingsBar;