import React from 'react';
import { render, RenderResult, screen } from '@testing-library/react';
import SettingsBar from './SettingsBar';

describe('SettingsBar', () => {
    let component: RenderResult;
    const scaling = '20';
    const setScaling = jest.fn();
    const changeDifficulty = jest.fn();

    beforeEach(() => {
        component = render(
            <SettingsBar
                scaling={scaling}
                setScaling={setScaling}
                scalingOptions={[scaling]}
                changeDifficulty={changeDifficulty}
            />
        );
    });

    it('has rendered all children properly', () => {
        expect(screen.getAllByText(scaling).length).toBeGreaterThan(0);
        expect(screen.getByText('Beginner')).toBeInTheDocument();
        expect(screen.getByText('Intermediate')).toBeInTheDocument();
        expect(screen.getByText('Expert')).toBeInTheDocument();
    });

    it('clicking the difficulty options triggers the callback', () => {
        screen.getByText('Beginner').click();
        expect(changeDifficulty).toHaveBeenCalledWith(0);
        screen.getByText('Intermediate').click();
        expect(changeDifficulty).toHaveBeenCalledWith(1);
        screen.getByText('Expert').click();
        expect(changeDifficulty).toHaveBeenCalledWith(2);
    });
});