import React from 'react';
import STYLES from './SettingsBarEntry.module.css';

interface SettingsBarEntryProps {
    children: JSX.Element[] | JSX.Element | string
}

const SettingsBarEntry: React.FunctionComponent<SettingsBarEntryProps> = ({
    children
}: SettingsBarEntryProps) => {
    return (
        <div className={STYLES.settingsBarEntry}>
            {children}
        </div>
    );
};

export default SettingsBarEntry;