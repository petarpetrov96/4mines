import React from 'react';
import { Position } from '../utils/gameState';
import GameStats from './GameStats';
import STYLES from './Mineground.module.css';
import MineRow from './MineRow';

interface MinegroundProps {
    rows: number,
    cols: number,
    positions: Position[][],
    scaling: number,
    openPosition: (rowId: number, colId: number) => void,
    flagPosition: (rowId: number, colId: number) => void,
    remainingMines: number,
    finishTimeString: string
}

const Mineground: React.FunctionComponent<MinegroundProps> = ({
    rows,
    cols,
    positions,
    scaling,
    openPosition,
    flagPosition,
    remainingMines,
    finishTimeString
}) => {
    return (
        <>
            <GameStats
                remainingMines={remainingMines}
                finishTimeString={finishTimeString}
            />
            <div className={STYLES.minegroundHolder}>
                {Array
                    .from(Array(rows).keys())
                    .map(rowId => (
                        <MineRow
                            key={`row${rowId}`}
                            rowId={rowId}
                            cols={cols}
                            positions={positions[rowId]}
                            scaling={scaling}
                            openPosition={openPosition}
                            flagPosition={flagPosition}
                        />
                    ))
                }
            </div>
        </>
    );
};

export default Mineground;