import React from 'react';
import { Position } from '../utils/gameState';
import MineCol from './MineCol';
import STYLES from './MineRow.module.css';

interface MineRowProps {
    rowId: number,
    cols: number,
    positions: Position[],
    scaling: number,
    openPosition: (rowId: number, colId: number) => void,
    flagPosition: (rowId: number, colId: number) => void
}

const MineRow: React.FunctionComponent<MineRowProps> = ({
    rowId,
    cols,
    positions,
    scaling,
    openPosition,
    flagPosition
}: MineRowProps) => {
    return (
        <div className={STYLES.mineRow}>
            {Array
                .from(Array(cols).keys())
                .map(colId => {
                    const { isMine, isOpen, isFlagged, neighbours } = positions[colId];
                    return (
                        <MineCol
                            key={`element${rowId}${colId}`}
                            rowId={rowId}
                            colId={colId}
                            isOpen={isOpen}
                            isMine={isMine}
                            isFlagged={isFlagged}
                            neighbours={neighbours}
                            scaling={scaling}
                            openPosition={openPosition}
                            flagPosition={flagPosition}
                        />
                )})
            }
        </div>
    );
};

export default MineRow;