import React from 'react';
import { BugOutline, FlagOutline } from 'react-ionicons';
import STYLES from './MineCol.module.css';

interface MineColProps {
    rowId: number,
    colId: number,
    isOpen: boolean,
    isMine: boolean,
    isFlagged: boolean,
    neighbours: number,
    scaling: number,
    openPosition: (rowId: number, colId: number) => void,
    flagPosition: (rowId: number, colId: number) => void
}

const MineCol: React.FunctionComponent<MineColProps> = ({
    rowId,
    colId,
    isOpen,
    isMine,
    isFlagged,
    neighbours,
    scaling,
    openPosition,
    flagPosition
}: MineColProps) => {
    const closedClass = `${STYLES.mineCol} ${STYLES.closed}`;

    const onClickHandler = (): void => {
        if(!isFlagged) {
            if(isOpen) {
                openPosition(rowId-1, colId);
                openPosition(rowId+1, colId);
                openPosition(rowId, colId-1);
                openPosition(rowId, colId+1);
            } else {
                openPosition(rowId, colId);
            }
        }
    }

    const onRightClickHandler = (event: React.MouseEvent): void => {
        event.preventDefault();
        if (!isOpen) {
            flagPosition(rowId, colId);
        }
    };

    return (
        <div
            className={isOpen ? STYLES.mineCol : closedClass}
            style={{
                width: `${scaling*2}px`,
                height: `${scaling*2}px`,
                fontSize: `${scaling}px`
            }}
            onClick={onClickHandler}
            onContextMenu={onRightClickHandler}
            data-testid="mineCol"
        >
            {(isOpen || isFlagged) ? <div
                className={STYLES.mineColNumber}
                style={{
                    marginTop: `-${Math.floor(scaling/2)}px`,
                    height: `${scaling}px`
                }}
            >
                {isFlagged
                    ? <FlagOutline
                        width={`${scaling}px`}
                        height={`${scaling}px`}
                        color="black"
                        title="Flag"
                    />
                    : isMine
                        ? <BugOutline
                            width={`${scaling}px`}
                            height={`${scaling}px`}
                            color="black"
                            title="Bug"
                        />
                        : (neighbours>0 ? neighbours : '')
                }
            </div> : null}
        </div>
    );
};

export default MineCol;