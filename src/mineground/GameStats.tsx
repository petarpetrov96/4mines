import React from 'react';
import STYLES from './GameStats.module.css';

interface GameStatsProps {
    remainingMines: number,
    finishTimeString: string
}

const GameStats: React.FunctionComponent<GameStatsProps> = ({
    remainingMines,
    finishTimeString
}: GameStatsProps) => {
    return (
        <>
            <div className={STYLES.gameStat}>Remaining mines: <span className={STYLES.bold}>{remainingMines}</span></div>
            {finishTimeString ? <div className={STYLES.gameStat}>Finish time: <span className={STYLES.bold}>{finishTimeString}</span></div> : null }
        </>
    )
};

export default GameStats;