import { render, screen, RenderResult } from "@testing-library/react";
import GameStats from "./GameStats";

describe('GameStats', () => {
    let component: RenderResult;
    it('renders correctly', () => {
        component = render(<GameStats remainingMines={20} finishTimeString="finishTimeString" />);
        expect(screen.getByText('20')).toBeInTheDocument();
        expect(screen.getByText('finishTimeString')).toBeInTheDocument();
    });
});