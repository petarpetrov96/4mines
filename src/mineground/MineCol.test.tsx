import { render, screen } from "@testing-library/react";
import MineCol from "./MineCol";

const rowId = 7;
const colId = 11;
const openPosition = jest.fn();
const flagPosition = jest.fn();
const renderMineCol = (isOpen: boolean, isMine: boolean, isFlagged: boolean, neighbours: number) => {
    return render(
        <MineCol
            rowId={rowId}
            colId={colId}
            isOpen={isOpen}
            isMine={isMine}
            isFlagged={isFlagged}
            neighbours={neighbours}
            scaling={20}
            openPosition={openPosition}
            flagPosition={flagPosition}
        />
    );
}

describe('MineCol', () => {
    it('renders correctly in normal mode', () => {
        const component = renderMineCol(false, false, false, 3);
        expect(screen.queryByTitle('Flag')).toBeNull();
        expect(screen.queryByTitle('Bug')).toBeNull();
        expect(screen.queryByText('3')).toBeNull();
    });

    it('renders correctly when flagged', () => {
        const component = renderMineCol(false, false, true, 3);
        expect(screen.getByTitle('Flag')).toBeInTheDocument();
        expect(screen.queryByTitle('Bug')).toBeNull();
        expect(screen.queryByText('3')).toBeNull();
    });

    it('renders correctly when opened with no mine', () => {
        const component = renderMineCol(true, false, false, 3);
        expect(screen.queryByTitle('Flag')).toBeNull();
        expect(screen.queryByTitle('Bug')).toBeNull();
        expect(screen.getByText('3')).toBeInTheDocument();
    });

    it('renders correctly when opened with no mine and no neighbours', () => {
        const component = renderMineCol(true, false, false, 0);
        expect(screen.queryByTitle('Flag')).toBeNull();
        expect(screen.queryByTitle('Bug')).toBeNull();
        expect(screen.queryByText('3')).toBeNull();
    });

    it('renders correctly when opened with a mine', () => {
        const component = renderMineCol(true, true, false, 3);
        expect(screen.queryByTitle('Flag')).toBeNull();
        expect(screen.getByTitle('Bug')).toBeInTheDocument();
        expect(screen.queryByText('3')).toBeNull();
    });

    it('click handler when flagged does not do anything', () => {
        const component = renderMineCol(false, false, true, 3);
        screen.getByTestId('mineCol').click();
        expect(openPosition).not.toHaveBeenCalled();
    });

    it('click handler behaves correctly when opening for the first time', () => {
        const component = renderMineCol(false, false, false, 3);
        screen.getByTestId('mineCol').click();
        expect(openPosition).toHaveBeenCalledWith(rowId, colId);
    });

    it('click handler behaves correctly when opened', () => {
        const component = renderMineCol(true, false, false, 3);
        screen.getByTestId('mineCol').click();
        expect(openPosition).toHaveBeenCalledWith(rowId-1, colId);
        expect(openPosition).toHaveBeenCalledWith(rowId+1, colId);
        expect(openPosition).toHaveBeenCalledWith(rowId, colId-1);
        expect(openPosition).toHaveBeenCalledWith(rowId, colId+1);
    });

    it('right click handler does not do anything when opened', () => {
        const component = renderMineCol(true, false, false, 3);
        var evt = new MouseEvent('contextmenu', {
            bubbles: true,
            cancelable: true,
            view: window,
            buttons: 2
          });
        screen.getByTestId('mineCol').dispatchEvent(evt);
        expect(flagPosition).not.toHaveBeenCalled();
    });

    it('right click handler flags when not opened', () => {
        const component = renderMineCol(false, false, false, 3);
        var evt = new MouseEvent('contextmenu', {
            bubbles: true,
            cancelable: true,
            view: window,
            buttons: 2
          });
        screen.getByTestId('mineCol').dispatchEvent(evt);
        expect(flagPosition).toHaveBeenCalledWith(rowId, colId);
    });
});