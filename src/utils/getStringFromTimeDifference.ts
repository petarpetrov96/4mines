const getStringFromTimeDiffence = (startTime: Date, finishTime: Date | null): string => {
    if (!finishTime) return '';
    const difference = finishTime.getTime() - startTime.getTime();
    const msDifference = difference%1000;
    const secDifference = Math.floor(difference/1000);
    return `${secDifference}s ${msDifference}ms`;
};

export default getStringFromTimeDiffence;