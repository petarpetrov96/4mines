import { GameState, Position } from "./gameState";
import { flagPosition, openPosition } from "./positionActions";

const gameState: GameState = {
    startTime: new Date(),
    finished: false,
    finishTime: new Date(),
    rows: 10,
    cols: 10,
    positions: []
}

describe('openPosition', () => {
    const setGameState = jest.fn();

    it('does not do anything when the game has finished', () => {
        openPosition(0, 0, {
            ...gameState,
            finished: true
        }, setGameState);
        expect(setGameState).not.toHaveBeenCalled();
    });

    it('does not do anything when the rowId or colId is out of range', () => {
        openPosition(-1, 1, gameState, setGameState);
        openPosition(11, 1, gameState, setGameState);
        openPosition(1, -1, gameState, setGameState);
        openPosition(1, 11, gameState, setGameState);
        expect(setGameState).not.toHaveBeenCalled();
    });

    it('does not do anything when the cell is opened or flagged', () => {
        openPosition(0, 0, {
            ...gameState,
            positions: [[{
                isOpen: true,
                isMine: false,
                isFlagged: false,
                neighbours: 0
            }]]
        }, setGameState);
        openPosition(0, 0, {
            ...gameState,
            positions: [[{
                isOpen: false,
                isMine: false,
                isFlagged: true,
                neighbours: 0
            }]]
        }, setGameState);
        expect(setGameState).not.toHaveBeenCalled();
    });

    it('sets the game state normally', () => {
        openPosition(0, 0, {
            ...gameState,
            positions: [[{
                isOpen: false,
                isMine: false,
                isFlagged: false,
                neighbours: 1
            }]]
        }, setGameState);
        expect(setGameState).toHaveBeenCalled();
    });

    it('sets the game state normally when the cell has 0 neighbours with mines', () => {
        const workingPosition: Position = {
            isOpen: false,
            isMine: false,
            isFlagged: false,
            neighbours: 1
        };
        openPosition(0, 0, {
            ...gameState,
            positions: [[{...workingPosition, neighbours: 0}, workingPosition],[workingPosition]]
        }, setGameState);
        expect(setGameState).toHaveBeenCalled();
    });

    it('sets the game state normally when a mine is encountered', () => {
        openPosition(0, 0, {
            ...gameState,
            positions: [[{
                isOpen: false,
                isMine: true,
                isFlagged: false,
                neighbours: 0
            }]]
        }, setGameState);
        expect(setGameState).toHaveBeenCalled();
    });
});

describe('flagPosition', () => {
    const setGameState = jest.fn();

    it('does not do anything when the game is finished', () => {
        flagPosition(0, 0, {
            ...gameState,
            finished: true
        }, setGameState);
        expect(setGameState).not.toHaveBeenCalled();
    });

    it('sets the game state normally', () => {
        flagPosition(0, 0, {
            ...gameState,
            positions: [[{
                isOpen: false,
                isMine: true,
                isFlagged: false,
                neighbours: 0
            }]]
        }, setGameState);
        expect(setGameState).toHaveBeenCalled();
    });
});