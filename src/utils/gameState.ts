import { compareCoordinatesEqual, getRandomisedMinePositions } from "./minePositions";

export interface Coordinate {
  rowId: number,
  colId: number
}

export interface Position {
    isOpen: boolean,
    isMine: boolean,
    isFlagged: boolean,
    neighbours: number
}

export interface GameState {
  startTime: Date,
  finished: boolean,
  finishTime: Date | null,
  rows: number,
  cols: number,
  positions: Position[][]
}

export const getInitialGameState = (rows: number, cols: number, mines: number): GameState => {
    const minePositions = getRandomisedMinePositions(rows, cols, mines);

    const positions: Position[][] = [];

    let i: number, j: number;
    for (i=0; i<rows; i++) {
        const rowPositions: Position[] = [];

        for (j=0; j<rows; j++) {
            const currentRowId = i;
            const currentColId = j;

            const isMine = minePositions.some((position) => compareCoordinatesEqual(position, {rowId: currentRowId, colId: currentColId}));

            const isLeftNeighbourMine = minePositions.some((position) => compareCoordinatesEqual(position, {rowId: currentRowId-1, colId: currentColId}));
            const isTopNeighbourMine = minePositions.some((position) => compareCoordinatesEqual(position, {rowId: currentRowId, colId: currentColId-1}));
            const isRightNeighbourMine = minePositions.some((position) => compareCoordinatesEqual(position, {rowId: currentRowId+1, colId: currentColId}));
            const isBottomNeighbourMine = minePositions.some((position) => compareCoordinatesEqual(position, {rowId: currentRowId, colId: currentColId+1}));

            const neighbours = (isLeftNeighbourMine ? 1 : 0)
                + (isTopNeighbourMine ? 1 : 0)
                + (isRightNeighbourMine ? 1 : 0)
                + (isBottomNeighbourMine ? 1 : 0);

            const position: Position = {
                isMine,
                isOpen: false,
                isFlagged: false,
                neighbours
            };
            rowPositions.push(position);
        }
        positions.push(rowPositions);
    }

    return {
        startTime: new Date(),
        finished: false,
        finishTime: null,
        rows,
        cols,
        positions
    };
};