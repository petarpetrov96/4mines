import getStringFromTimeDiffence from "./getStringFromTimeDifference";

describe('getStringFromTimeDifference', () => {
    it('does not return anything when game has not finished yet', () => {
        const result = getStringFromTimeDiffence(new Date(), null);
        expect(result).toEqual('');
    });

    it('returns the correct interval when given two dates', () => {
        const startDate = new Date(2021, 9, 6, 11, 25, 30, 350);
        const endDate = new Date(2021, 9, 6, 11, 25, 31, 500);
        const result = getStringFromTimeDiffence(startDate, endDate);
        expect(result).toEqual('1s 150ms');
    })
});