import { GameState } from "./gameState";

export const openPosition = (rowId: number, colId: number, gameState: GameState, setGameState: (gameState: GameState) => void) => {
    if (gameState.finished) return;
    if (rowId < 0 || rowId >= gameState.rows || colId < 0 || colId >= gameState.cols) return;

    const { positions } = gameState;
    if (positions[rowId][colId].isOpen || positions[rowId][colId].isFlagged) return;

    positions[rowId][colId].isOpen = true;
    const finished = positions[rowId][colId].isMine;
    const finishTime = finished ? new Date() : null;

    setGameState({
        ...gameState,
        positions,
        finished,
        finishTime
    });

    if (!finished && positions[rowId][colId].neighbours === 0) {
        openPosition(rowId-1, colId, gameState, setGameState);
        openPosition(rowId+1, colId, gameState, setGameState);
        openPosition(rowId, colId-1, gameState, setGameState);
        openPosition(rowId, colId+1, gameState, setGameState);
    }
};

export const flagPosition = (rowId: number, colId: number, gameState: GameState, setGameState: (gameState: GameState) => void) => {
    if (gameState.finished) return;

    const { positions } = gameState;
    positions[rowId][colId].isFlagged = !positions[rowId][colId].isFlagged;

    setGameState({
        ...gameState,
        positions
    });
};