import { Coordinate } from "./gameState";

export const compareCoordinatesEqual = (coordinate1: Coordinate, coordinate2: Coordinate): boolean => {
    return (coordinate1.rowId === coordinate2.rowId && coordinate1.colId === coordinate2.colId);
};

export const getRandomisedMinePositions = (rows: number, cols: number, mines: number): Coordinate[] => {
    const minePositions: Coordinate[] = [];
    while (minePositions.length < mines) {
        const randomRowId = Math.round(Math.random() * (rows-1));
        const randomColId = Math.round(Math.random() * (cols-1));

        const randomCoordinate: Coordinate = {
            rowId: randomRowId,
            colId: randomColId
        };
        
        if (!minePositions.some((position) => compareCoordinatesEqual(position, randomCoordinate))) {
            minePositions.push(randomCoordinate);
        }
    }
    return minePositions;
};