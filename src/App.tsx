import React, { useEffect, useState } from 'react';
import STYLES from './App.module.css';
import SettingsBar from './components/SettingsBar';
import Mineground from './mineground/Mineground';
import { getInitialGameState } from './utils/gameState';
import getStringFromTimeDifference from './utils/getStringFromTimeDifference';
import { flagPosition, openPosition } from './utils/positionActions';

const difficulties = [
  {
    rows: 10,
    cols: 10,
    mines: 20
  },
  {
    rows: 15,
    cols: 15,
    mines: 45
  },
  {
    rows: 25,
    cols: 25,
    mines: 75
  }
];
const scalingOptions = ['12', '14', '16', '18', '20', '24', '28', '32']

const App: React.FunctionComponent = () => {
  const [scaling, setScaling] = useState('20');
  const { rows: initialRows, cols: initialCols, mines: initialMines} = difficulties[0];
  const [gameState, setGameState] = useState(getInitialGameState(initialRows, initialCols, initialMines));

  const changeDifficulty = (difficulty: number) => {
    const { rows, cols, mines } = difficulties[difficulty];
    setGameState(getInitialGameState(rows, cols, mines));
  };

  const openPositionWrapper = (rowId: number, colId: number) => {
    openPosition(rowId, colId, gameState, setGameState);
  }

  const flagPositionWrapper = (rowId: number, colId: number) => {
    flagPosition(rowId, colId, gameState, setGameState);
  }

  let i: number, j: number;
  let totalFlagged = 0;
  let totalMines = 0;
  let leftToOpen = gameState.rows*gameState.cols;
  for (i=0; i<gameState.rows; i++) {
    for (j=0; j<gameState.cols; j++) {
      const { isMine, isFlagged, isOpen } = gameState.positions[i][j];

      if (isMine) totalMines++;
      if (isFlagged) totalFlagged++;
      if (isOpen || isFlagged) leftToOpen--;
    }
  }

  useEffect(() => {
    if (leftToOpen === 0 && !gameState.finished) {
      setGameState({
        ...gameState,
        finished: true,
        finishTime: new Date()
      });
    }
  }, [leftToOpen, gameState]);
  
  const remainingMines = totalMines - totalFlagged;

  return (
    <div className={STYLES.pageContainer} data-testid="pageContainer">
      <SettingsBar
        scaling={scaling}
        setScaling={setScaling}
        scalingOptions={scalingOptions}
        changeDifficulty={changeDifficulty}
      />
      <Mineground
        rows={gameState.rows}
        cols={gameState.cols}
        positions={gameState.positions}
        scaling={parseInt(scaling)}
        openPosition={openPositionWrapper}
        flagPosition={flagPositionWrapper}
        remainingMines={remainingMines}
        finishTimeString={getStringFromTimeDifference(gameState.startTime, gameState.finishTime)}
      />
    </div>
  );
};

export default App;