import React  from 'react';
import { ChevronDownCircleOutline, ChevronUpCircleOutline } from 'react-ionicons';
import STYLES from './Dropdown.module.css';

type CustomIconType = React.FunctionComponent<{cssClasses: string, title: string}>;

interface DropdownProps {
    values: string[],
    selectedValue: string,
    setSelectedValue: (value: string) => void,
    CustomIcon?: CustomIconType,
    title?: string
}

const Dropdown: React.FunctionComponent<DropdownProps> = ({
    values,
    selectedValue,
    setSelectedValue,
    CustomIcon,
    title
}: DropdownProps) => {
    const [isOpen, setOpen] = React.useState(false);

    const selectorClickHandler = () => {
        if (isOpen) {
            setOpen(false);
        } else {
            setOpen(true);
        }
    };

    const optionClickHandler = (optionValue: string) => {
        setSelectedValue(optionValue);
        setOpen(false);
    };

    return (
        <div className={STYLES.dropdownHolder}>
            <div className={STYLES.dropdownSelector} title={title} onClick={selectorClickHandler}>
                {CustomIcon
                    ? <CustomIcon cssClasses={STYLES.dropdownSelectorCustomIcon} title="Custom Icon" />
                    : null}
                <span>{selectedValue}</span>
                {isOpen
                    ? <ChevronUpCircleOutline cssClasses={STYLES.dropdownSelectorIcon} title="Up Icon" />
                    : <ChevronDownCircleOutline cssClasses={STYLES.dropdownSelectorIcon} title="Down Icon" />
                }
            </div>
            <div className={isOpen ? `${STYLES.dropdownOptions} ${STYLES.open}` : STYLES.dropdownOptions}>
                {values.map((value, index) => (
                    <div key={`option${index}`} className={STYLES.dropdownOption} onClick={() => optionClickHandler(value)}>
                        {value}
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Dropdown;