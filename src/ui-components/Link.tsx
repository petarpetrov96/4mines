import React from 'react';
import STYLES from './Link.module.css';

interface LinkProps {
    children: JSX.Element | string
    href?: string,
    onClick?: () => void,
}

const Link: React.FunctionComponent<LinkProps> = ({
    children,
    href,
    onClick
}: LinkProps) => {
    return (<a href={href} onClick={onClick} className={STYLES.link}>{children}</a>)
};

export default Link;