import { render, screen } from "@testing-library/react";
import { AlertOutline } from "react-ionicons";
import Dropdown from "./Dropdown";
import React from 'react';

const setSelectedValue = jest.fn();
const renderDropdown = (selectedValue: string, values: string[], title?: string) => {
    return render(
        <Dropdown
            selectedValue={selectedValue}
            setSelectedValue={setSelectedValue}
            values={values}
            title={title}
        />
    );
};

const renderDropdownWithCustomIcon = (selectedValue: string, values: string[], title?: string) => {
    return render(
        <Dropdown
            selectedValue={selectedValue}
            setSelectedValue={setSelectedValue}
            values={values}
            title={title}
            CustomIcon={AlertOutline}
        />
    );
};

describe('Dropdown', () => {
    const setOpen = jest.fn();
    it('renders correctly', () => {
        renderDropdown('Option 2', ['Option 1', 'Option 2', 'Option 3']);
        expect(screen.getByText('Option 1')).toBeInTheDocument();
        expect(screen.getAllByText('Option 2').length).toBe(2);
        expect(screen.getByText('Option 3')).toBeInTheDocument();
    });

    it('renders correctly with custom icon', () => {
        renderDropdownWithCustomIcon('Option 2', ['Option 1', 'Option 2', 'Option 3']);
        expect(screen.getByText('Option 1')).toBeInTheDocument();
        expect(screen.getAllByText('Option 2').length).toBe(2);
        expect(screen.getByText('Option 3')).toBeInTheDocument();
        expect(screen.getByTitle('Custom Icon')).toBeInTheDocument();
    });

    it('calls the callback when clicking the options', () => {
        renderDropdown('Option 2', ['Option 1', 'Option 2', 'Option 3']);
        screen.getByText('Option 1').click();
        screen.getAllByText('Option 2')[1].click();
        screen.getByText('Option 3').click();
    });

    it('changes the state correctly from true to false', () => {
        jest.spyOn(React, 'useState').mockReturnValue([true, setOpen]);
        renderDropdown('Option 2', ['Option 1', 'Option 2', 'Option 3'], 'TestTitle');
        screen.getByTitle('TestTitle').click();
        expect(setOpen).toHaveBeenCalledWith(false);
    });

    it('changes the state correctly from false to true', () => {
        jest.spyOn(React, 'useState').mockReturnValue([false, setOpen]);
        renderDropdown('Option 2', ['Option 1', 'Option 2', 'Option 3'], 'TestTitle');
        screen.getByTitle('TestTitle').click();
        expect(setOpen).toHaveBeenCalledWith(true);
    });
});